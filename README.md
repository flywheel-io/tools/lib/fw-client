# fw-client

Flywheel HTTP JSON API client hardened for production with transport and HTTP error retries.

## Installation

```bash
# installing into a venv
pip install fw-client

# adding as a project dependency
poetry add fw-client
```

## Usage

```python
from fw_client import FWClient
fw = FWClient("site.flywheel.io:djEuU79cNFk8pk4tLwxybb1YHqhzhyEXAMPLE")
project_labels = [project.label for project in fw.get("/api/projects")]
```

## Linting

```bash
pre-commit run -a
```

## Testing

```bash
poetry install
poetry run pytest
```

## License

[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)

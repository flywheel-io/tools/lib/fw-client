import pytest

from fw_client.client import DRONE_DEVICE_KEYS

pytest_plugins = "fw_http_testserver"


@pytest.fixture(scope="session")
def anyio_backend():
    return "asyncio"


@pytest.fixture(autouse=True)
def clear_state():
    DRONE_DEVICE_KEYS.clear()

"""Test FWClient input validation and initialization."""

import io
import time
from functools import partial

import flask
import pytest

from fw_client import Event, FWClient, errors


def test_api_key_with_prefix():
    client = FWClient(api_key="scitran-user host:key")
    assert client.base_url == client._async.base_url == "https://host"
    assert client.headers == client._async.headers
    assert client.headers["Authorization"] == "scitran-user key"


def test_api_key_with_embedded_host():
    client = FWClient(api_key="host:key")
    assert client.base_url == "https://host"
    assert client.headers["Authorization"] == "scitran-user key"


def test_enhanced_api_key_used_as_bearer_token():
    enhanced_key = "k" * 57  # new keys are always 57 chars long
    client = FWClient(api_key=f"host:{enhanced_key}")
    assert client.headers["Authorization"] == f"Bearer {enhanced_key}"


def test_api_key_with_embedded_host_and_custom_port():
    client = FWClient(api_key="host:8443:key")
    assert client.base_url == "https://host:8443"
    assert client.headers["Authorization"] == "scitran-user key"


def test_api_key_with_embedded_host_and_custom_port_using_http():
    client = FWClient(api_key="http://host:8080:key")
    assert client.base_url == "http://host:8080"
    assert client.headers["Authorization"] == "scitran-user key"


def test_api_key_without_embedded_host():
    client = FWClient(api_key="key", url="host/")
    assert client.base_url == "https://host"
    assert client.headers["Authorization"] == "scitran-user key"


def test_api_key_w_domain_or_base_url_required():
    error = r".*api_key with domain or base_url required.*"
    with pytest.raises(errors.ValidationError, match=error):
        FWClient()


def test_api_key_without_embedded_host_and_url():
    client = FWClient(api_key="key", url="https://host")
    assert client.base_url == "https://host"
    assert client.headers["Authorization"] == "scitran-user key"


@pytest.fixture
def core(http_testserver):
    return http_testserver


def test_drone_secret(core):
    core.add_response("/api/config", {"key": "value"})
    core.add_response("/api/devices", [{"_id": 1}, {"_id": 2}])
    core.add_response("/api/devices/2", method="DELETE")
    core.add_response("/api/devices/self", {"key": "key"})
    client = FWClient(
        url=core.url,
        device_type="type",
        device_label="label",
        drone_secret="secret",  # noqa S106
    )
    assert core.request_log == [
        "GET /api/devices",  # list devices 1st (w/ filters)
        "DELETE /api/devices/2",  # delete all but the 1st
        "GET /api/devices/self",  # finally, get the key
    ]
    get_devices = core.requests[0]
    assert get_devices.headers["X-Scitran-Auth"] == "secret"
    assert get_devices.headers["X-Scitran-Method"] == "type"
    assert get_devices.headers["X-Scitran-Name"] == "label"
    assert client.headers["Authorization"] == "scitran-user key"


def test_auth_missing_raises():
    error = r".*api_key or drone_secret required.*"
    with pytest.raises(errors.ValidationError, match=error):
        FWClient(url="https://host/api")


def test_defer_auth(core):
    client = FWClient(url=core.url, defer_auth=True)
    assert "Authorization" not in client.headers

    core.add_response("/api/test")
    client.get("/api/test", auth="token")
    assert core.request_log == ["GET /api/test"]
    assert core.requests[0].headers["Authorization"] == "token"


@pytest.mark.anyio
async def test_async_defer_auth(core):
    client = FWClient(url=core.url, defer_auth=True)
    assert "Authorization" not in client.headers

    core.add_response("/api/test")
    await client.aget("/api/test", auth="token")
    assert core.request_log == ["GET /api/test"]
    assert core.requests[0].headers["Authorization"] == "token"


def test_defer_auth_raises_if_credential_specified(core):
    error = r".*api_key and drone_secret not allowed with defer_auth.*"
    with pytest.raises(errors.ValidationError, match=error):
        FWClient(url=core.url, api_key="host:key", defer_auth=True)
    with pytest.raises(errors.ValidationError, match=error):
        FWClient(url=core.url, drone_secret="secret", defer_auth=True)


def test_anon_request_with_authorization_none(core):
    core.add_response("/api/test")
    client = FWClient(url=core.url, api_key="host:key")
    client.get("/api/test", headers={"Authorization": None})
    assert core.request_log == ["GET /api/test"]
    assert "Authorization" not in core.requests[0].headers


def test_alternative_service_url(core):
    client = FWClient(url=core.url, api_key="key", xfer_url=f"{core.url}/xfer_url")
    core.add_response("/xfer_url/xfer/test")
    client.get("/xfer/test")
    assert core.request_log == ["GET /xfer_url/xfer/test"]


@pytest.fixture
def client(core) -> FWClient:
    """Return FWClient configured for the HTTP test server."""
    return FWClient(api_key=f"{core.url}:key")


def test_update_headers(client):
    client.headers = {"X-Test": "value"}
    assert client.headers["X-Test"] == "value"
    assert client._async.headers["X-Test"] == "value"


def test_check_feature(client, core):
    core.add_response("/api/config", {"features": {"foo": True}})

    assert client.check_feature("foo") is True
    assert client.check_feature("bar") is False
    assert core.request_log == ["GET /api/config"]


def test_check_version(client, core):
    core.add_response("/api/version", {"flywheel_release": "1.2.3"})

    assert client.check_version("1.2.2") is True
    assert client.check_version("1.2.3") is True
    assert client.check_version("1.2.4") is False
    assert core.request_log == ["GET /api/version"]


def test_device_status(client, core):
    status = {"is_device": True, "origin": {"id": "123"}}
    core.add_response("/api/auth/status", status)
    core.add_response("/api/devices/self", {"foo": "bar"})

    assert client.auth_status == {
        "is_device": True,
        "origin": {"id": "123"},
        "info": {"foo": "bar"},
    }
    assert core.request_log == ["GET /api/auth/status", "GET /api/devices/self"]


def test_user_status(client, core):
    core.add_response("/api/auth/status", {"is_device": False})
    core.add_response("/api/users/self", {"bar": "baz"})

    assert client.auth_status == {
        "is_device": False,
        "info": {"bar": "baz"},
    }
    assert core.request_log == ["GET /api/auth/status", "GET /api/users/self"]


def test_raw_response(client, core):
    core.add_response("/api/foo", {"key": "value"})
    response = client.get("/api/foo", raw=True)
    assert response.status_code == 200
    assert response.json() == {"key": "value"}


@pytest.mark.anyio
async def test_async_raw_response(client, core):
    core.add_response("/api/foo", {"key": "value"})
    response = await client.aget("/api/foo", raw=True)
    assert response.status_code == 200
    assert response.json() == {"key": "value"}


def test_stream_response(client, core):
    core.add_response("/api/foo", b"foobar")
    # requests style
    with client.stream("GET", "/api/foo") as response:
        assert response.status_code == 200
        assert response.raw.readable() is True
        assert response.raw.read() == b"foobar"


def test_stream_response_read_chunks(client, core):
    core.add_response("/api/foo", b"foobar")
    with client.stream("GET", "/api/foo") as response:
        assert response.status_code == 200
        assert response.raw.readable() is True
        assert response.raw.read(4) == b"foob"
        assert response.raw.read(4) == b"ar"


@pytest.mark.anyio
async def test_stream_response_aread_raises_on_sync(client, core):
    core.add_response("/api/foo", b"foobar")
    with client.stream("GET", "/api/foo") as response:
        assert response.status_code == 200
        expected = "Attempted to call an async iterator on a sync stream."
        with pytest.raises(RuntimeError, match=expected):
            await response.raw.aread()


@pytest.mark.anyio
async def test_async_stream_response(client, core):
    core.add_response("/api/foo", b"foobar")
    # httpx style
    async with client.astream("GET", "/api/foo") as response:
        assert response.status_code == 200
        assert response.raw.readable() is True
        assert (await response.raw.aread()) == b"foobar"


@pytest.mark.anyio
async def test_async_stream_response_read_chunks(client, core):
    core.add_response("/api/foo", b"foobar")
    async with client.astream("GET", "/api/foo") as response:
        assert response.status_code == 200
        assert response.raw.readable() is True
        assert await response.raw.aread(4) == b"foob"
        assert await response.raw.aread(4) == b"ar"


@pytest.mark.anyio
async def test_async_stream_response_aread_raises_on_sync(client, core):
    core.add_response("/api/foo", b"foobar")
    async with client.astream("GET", "/api/foo") as response:
        assert response.status_code == 200
        expected = "Attempted to call a sync iterator on an async stream."
        with pytest.raises(RuntimeError, match=expected):
            response.raw.read()


def test_upload_device_file_using_signed_url(client, core, tmp_path):
    status = {"is_device": True, "origin": {"id": "123", "type": "device"}}
    core.add_response("/api/auth/status", status)
    core.add_response("/api/devices/self", {"foo": "bar"})
    core.add_response("/signed-url", method="PUT")
    file_resp = {"storage_file_id": "file_id", "upload_url": f"{core.url}/signed-url"}
    core.add_response("/api/storage/files", file_resp, method="POST")

    file_id = client.upload_device_file("p_id", io.BytesIO(b"foo"))
    assert file_id == "file_id"
    assert core.request_map["PUT /signed-url"]["headers"]["Content-Length"] == "3"

    path = tmp_path / "foo"
    path.write_bytes(b"foo")
    with open(path, "rb") as file:
        file_id = client.upload_device_file("p_id", file)
    assert file_id == "file_id"
    assert core.request_map["PUT /signed-url"]["headers"]["Content-Length"] == "3"


def test_upload_device_file_fallback_to_direct_upload(client, core):
    status = {"is_device": True, "origin": {"id": "123", "type": "device"}}
    core.add_response("/api/auth/status", status)
    core.add_response("/api/devices/self", {"foo": "bar"})

    def callback():
        if core.request.args.get("signed_url"):
            return {}, 409
        return {"storage_file_id": "file_id"}, 200

    core.add_callback("/api/storage/files", callback, methods=["POST"])
    file = io.BytesIO(b"foo")
    f_id = client.upload_device_file("p_id", file, content_encoding="gzip")
    assert f_id == "file_id"
    assert core.requests[-2].headers["Content-Encoding"] == "gzip"


def test_upload_device_file_raises_if_not_device(client, core):
    core.add_response("/api/auth/status", {"is_device": False})
    core.add_response("/api/users/self", {"foo": "bar"})

    with pytest.raises(AssertionError, match="device authentication required"):
        client.upload_device_file("project_id", io.BytesIO(b"foo"))


def test_upload_device_file_raises_on_http_error(client, core):
    status = {"is_device": True, "origin": {"id": "123", "type": "device"}}
    core.add_response("/api/auth/status", status)
    core.add_response("/api/devices/self", {"foo": "bar"})
    core.add_response("/api/storage/files", method="POST", status=400)

    with pytest.raises(errors.ClientError):
        client.upload_device_file("project_id", io.BytesIO(b"foo"))


def test_upload_device_file_deletes_file_from_core_on_signed_url_error(client, core):
    status = {"is_device": True, "origin": {"id": "123", "type": "device"}}
    core.add_response("/api/auth/status", status)
    core.add_response("/api/devices/self", {"foo": "bar"})
    core.add_response("/signed-url", method="PUT", status=502)
    file_resp = {"storage_file_id": "file_id", "upload_url": f"{core.url}/signed-url"}
    core.add_response("/api/storage/files", file_resp, method="POST")
    core.add_response("/api/storage/files/file_id", {}, method="DELETE")

    with pytest.raises(errors.ServerError):
        client.upload_device_file("project_id", io.BytesIO(b"foo"))

    assert "DELETE /api/storage/files/file_id" in core.request_log


@pytest.mark.anyio
async def test_aupload_device_file_using_signed_url(client, core, tmp_path):
    status = {"is_device": True, "origin": {"id": "123", "type": "device"}}
    core.add_response("/api/auth/status", status)
    core.add_response("/api/devices/self", {"foo": "bar"})
    core.add_response("/signed-url", method="PUT")
    file_resp = {"storage_file_id": "file_id", "upload_url": f"{core.url}/signed-url"}
    core.add_response("/api/storage/files", file_resp, method="POST")

    file_id = await client.aupload_device_file("p_id", io.BytesIO(b"foo"))
    assert file_id == "file_id"
    assert core.request_map["PUT /signed-url"]["headers"]["Content-Length"] == "3"

    path = tmp_path / "foo"
    path.write_bytes(b"foo")
    with open(path, "rb") as file:
        file_id = await client.aupload_device_file("p_id", file)
    assert file_id == "file_id"
    assert core.request_map["PUT /signed-url"]["headers"]["Content-Length"] == "3"


@pytest.mark.anyio
async def test_aupload_device_file_fallback_to_direct_upload(client, core):
    status = {"is_device": True, "origin": {"id": "123", "type": "device"}}
    core.add_response("/api/auth/status", status)
    core.add_response("/api/devices/self", {"foo": "bar"})

    def callback():
        if core.request.args.get("signed_url"):
            return {}, 409
        return {"storage_file_id": "file_id"}, 200

    core.add_callback("/api/storage/files", callback, methods=["POST"])
    file = io.BytesIO(b"foo")
    f_id = await client.aupload_device_file("p_id", file, content_encoding="gzip")
    assert f_id == "file_id"
    assert core.requests[-2].headers["Content-Encoding"] == "gzip"


@pytest.mark.anyio
async def test_aupload_device_file_raises_if_not_device(client, core):
    core.add_response("/api/auth/status", {"is_device": False})
    core.add_response("/api/users/self", {"foo": "bar"})

    with pytest.raises(AssertionError, match="device authentication required"):
        await client.aupload_device_file("project_id", io.BytesIO(b"foo"))


@pytest.mark.anyio
async def test_aupload_device_file_raises_on_http_error(client, core):
    status = {"is_device": True, "origin": {"id": "123", "type": "device"}}
    core.add_response("/api/auth/status", status)
    core.add_response("/api/devices/self", {"foo": "bar"})
    core.add_response("/api/storage/files", method="POST", status=400)

    with pytest.raises(errors.ClientError):
        await client.aupload_device_file("project_id", io.BytesIO(b"foo"))


@pytest.mark.anyio
async def test_aupload_device_file_deletes_file_from_core_on_signed_url_error(
    client, core
):
    status = {"is_device": True, "origin": {"id": "123", "type": "device"}}
    core.add_response("/api/auth/status", status)
    core.add_response("/api/devices/self", {"foo": "bar"})
    core.add_response("/signed-url", method="PUT", status=502)
    file_resp = {"storage_file_id": "file_id", "upload_url": f"{core.url}/signed-url"}
    core.add_response("/api/storage/files", file_resp, method="POST")
    core.add_response("/api/storage/files/file_id", {}, method="DELETE")

    with pytest.raises(errors.ServerError):
        await client.aupload_device_file("project_id", io.BytesIO(b"foo"))

    assert "DELETE /api/storage/files/file_id" in core.request_log


def test_response_iter_lines(http_testserver, client):
    http_testserver.add_response("/api/lines", "line\n" * 10)
    with client.stream("GET", "/api/lines") as response:
        assert list(response.iter_lines()) == ["line"] * 10


def test_response_iter_jsonl(http_testserver, client):
    http_testserver.add_response("/api/jsonl", '{"foo":"bar"}\n' * 10)
    with client.stream("GET", "/api/jsonl") as response:
        assert list(response.iter_jsonl()) == [{"foo": "bar"}] * 10


@pytest.mark.anyio
async def test_async_response_iter_jsonl(http_testserver, client):
    http_testserver.add_response("/api/jsonl", '{"foo":"bar"}\n' * 10)
    async with client.astream("GET", "/api/jsonl") as response:
        results = [part async for part in response.aiter_jsonl()]
        assert results == [{"foo": "bar"}] * 10


MULTIPART_HEADER = {"content-type": "multipart/mixed;boundary=deadbeefdeadbeef"}
MULTIPART_BODY = b"""
ignored preamble
--deadbeefdeadbeef
content-disposition: form-data; name="field1"

implicit text/plain
--deadbeefdeadbeef
content-type: application/json

{"foo":"bar"}
--deadbeefdeadbeef
content-type: text/xml

<foo>bar</foo>
--deadbeefdeadbeef--
ignored epilogue
"""

MULTIPART_BODY = MULTIPART_BODY.replace(b"\n", b"\r\n")
EXPECTED_PARTS = [b"implicit text/plain", b'{"foo":"bar"}', b"<foo>bar</foo>"]


def test_response_iter_parts_yields_multipart_contents(http_testserver, client):
    http_testserver.add_response(
        "/api/multipart", MULTIPART_BODY, headers=MULTIPART_HEADER
    )
    with client.stream("GET", "/api/multipart") as response:
        assert [part.content for part in response.iter_parts()] == EXPECTED_PARTS


def test_response_iter_parts_with_starting_delimiter(http_testserver, client):
    body = MULTIPART_BODY.replace(b"\r\nignored preamble\r\n", b"")
    http_testserver.add_response("/api/multipart", body, headers=MULTIPART_HEADER)
    with client.stream("GET", "/api/multipart") as response:
        assert [part.content for part in response.iter_parts()] == EXPECTED_PARTS


def test_response_iter_parts_yields_full_msg_if_no_boundary(http_testserver, client):
    headers = {"content-type": "multipart"}
    http_testserver.add_response("/api/multipart", "content", headers=headers)
    with client.stream("GET", "/api/multipart") as response:
        assert [part.content for part in response.iter_parts()] == [b"content"]


def test_response_iter_parts_raises_on_invalid_content_type(http_testserver, client):
    http_testserver.add_response("/api/multipart", "content")
    with client.stream("GET", "/api/multipart") as response:
        with pytest.raises(ValueError):
            next(response.iter_parts())


def test_response_iter_parts_raises_on_missing_double_crlf(http_testserver, client):
    invalid_body = b"\r\n--deadbeefdeadbeef\r\nNO-CRLF\r\n--deadbeefdeadbeef"
    http_testserver.add_response(
        "/api/multipart", invalid_body, headers=MULTIPART_HEADER
    )
    with client.stream("GET", "/api/multipart") as response:
        with pytest.raises(ValueError):
            next(response.iter_parts())


def test_response_iter_parts_warns_without_closing_delimiter(http_testserver, client):
    invalid_body = b"\r\n--deadbeefdeadbeef\r\n\r\nPART\r\n--deadbeefdeadbeefNO--"
    http_testserver.add_response(
        "/api/multipart", invalid_body, headers=MULTIPART_HEADER
    )
    with client.stream("GET", "/api/multipart") as response:
        with pytest.warns(UserWarning):
            list(response.iter_parts())


@pytest.mark.anyio
async def test_async_response_iter_parts_yields_multipart_contents(
    http_testserver, client
):
    http_testserver.add_response(
        "/api/multipart", MULTIPART_BODY, headers=MULTIPART_HEADER
    )
    async with client.astream("GET", "/api/multipart") as response:
        result = [part.content async for part in response.aiter_parts()]
        assert result == EXPECTED_PARTS


@pytest.mark.anyio
async def test_async_response_iter_parts_with_starting_delimiter(
    http_testserver, client
):
    body = MULTIPART_BODY.replace(b"\r\nignored preamble\r\n", b"")
    http_testserver.add_response("/api/multipart", body, headers=MULTIPART_HEADER)
    async with client.astream("GET", "/api/multipart") as response:
        result = [part.content async for part in response.aiter_parts()]
        assert result == EXPECTED_PARTS


@pytest.mark.anyio
async def test_async_response_iter_parts_yields_full_msg_if_no_boundary(
    http_testserver, client
):
    headers = {"content-type": "multipart"}
    http_testserver.add_response("/api/multipart", "content", headers=headers)
    async with client.astream("GET", "/api/multipart") as response:
        result = [part.content async for part in response.aiter_parts()]
        assert result == [b"content"]


@pytest.mark.anyio
async def test_async_response_iter_parts_raises_on_invalid_content_type(
    http_testserver, client
):
    http_testserver.add_response("/api/multipart", "content")
    async with client.astream("GET", "/api/multipart") as response:
        with pytest.raises(ValueError):
            await anext(response.aiter_parts())


@pytest.mark.anyio
async def test_async_response_iter_parts_raises_on_missing_double_crlf(
    http_testserver, client
):
    invalid_body = b"\r\n--deadbeefdeadbeef\r\nNO-CRLF\r\n--deadbeefdeadbeef"
    http_testserver.add_response(
        "/api/multipart", invalid_body, headers=MULTIPART_HEADER
    )
    async with client.astream("GET", "/api/multipart") as response:
        with pytest.raises(ValueError):
            await anext(response.aiter_parts())


@pytest.mark.anyio
async def test_async_response_iter_parts_warns_without_closing_delimiter(
    http_testserver, client
):
    invalid_body = b"\r\n--deadbeefdeadbeef\r\n\r\nPART\r\n--deadbeefdeadbeefNO--"
    http_testserver.add_response(
        "/api/multipart", invalid_body, headers=MULTIPART_HEADER
    )
    async with client.astream("GET", "/api/multipart") as response:
        with pytest.warns(UserWarning):
            [part async for part in response.aiter_parts()]


SSE_HEADER = {"content-type": "text/event-stream"}
SSE_BODY = b"""
: comment only in block - no event

: event w/ id and multiple data lines showcasing whitespace handling
id: 1
data
data:
data:a
data: b
data:  c

: event w/ type, retry and : in data value
event: evt
data: foo:bar
retry: 1000

: empty id - resets the last event id w/o event
id

: invalid id/field/retry and incomplete block - all ignored
id: ignored\0 - id ignored if contains null-byte
foo: ignored - field ignored if not id|event|data|retry
retry: ignored - retry ignored if not isdigit
data: ignored - blocks only fire events if there's an empty line after
""".lstrip()

EXPECTED_EVENTS = [
    Event(id="1", data="\n\na\nb\n c"),
    Event(type="evt", data="foo:bar", retry=1000),
]


@pytest.mark.parametrize("eol", [b"\r\n", b"\n", b"\r"])
def test_response_iter_events_yields_sse_events(http_testserver, client, eol):
    body = SSE_BODY.replace(b"\n", eol)
    http_testserver.add_response("/api/events", body, headers=SSE_HEADER)
    with client.stream("GET", "/api/events") as response:
        assert list(response.iter_events(chunk_size=1)) == EXPECTED_EVENTS


def test_response_iter_events_raises_on_invalid_content_type(http_testserver, client):
    http_testserver.add_response("/api/events", SSE_BODY)
    with client.stream("GET", "/api/events") as response:
        with pytest.raises(ValueError):
            next(response.iter_events(chunk_size=1))


@pytest.mark.anyio
@pytest.mark.parametrize("eol", [b"\r\n", b"\n", b"\r"])
async def test_async_response_iter_events_yields_sse_events(
    http_testserver, client, eol
):
    body = SSE_BODY.replace(b"\n", eol)
    http_testserver.add_response("/api/events", body, headers=SSE_HEADER)
    async with client.astream("GET", "/api/events") as response:
        result = [event async for event in response.aiter_events(chunk_size=1)]
        assert result == EXPECTED_EVENTS


@pytest.mark.anyio
async def test_async_response_iter_events_raises_on_invalid_content_type(
    http_testserver, client
):
    http_testserver.add_response("/api/events", SSE_BODY)
    async with client.astream("GET", "/api/events") as response:
        with pytest.raises(ValueError):
            await anext(response.aiter_events(chunk_size=1))


def test_error_message(http_testserver, client):
    http_testserver.add_response("/api/400", {"foo": "bar"}, method="POST", status=400)
    expected_error = (
        rf"POST {http_testserver.url}/api/400\?param=1 - 400 BAD REQUEST"
        '\nResponse: {"foo":"bar"}'
    )
    with pytest.raises(errors.ClientError, match=expected_error):
        client.post(
            "/api/400",
            headers={"Authorization": "Bearer foo", "X-Auth": "barbaz"},
            params={"param": 1},
            json={"bar": "baz"},
        )


@pytest.mark.anyio
async def test_async_error_message(http_testserver, client):
    http_testserver.add_response("/api/400", {"foo": "bar"}, method="POST", status=400)
    expected_error = (
        rf"POST {http_testserver.url}/api/400\?param=1 - 400 BAD REQUEST"
        '\nResponse: {"foo":"bar"}'
    )
    with pytest.raises(errors.ClientError, match=expected_error):
        await client.apost(
            "/api/400",
            headers={"Authorization": "Bearer foo", "X-Auth": "barbaz"},
            params={"param": 1},
            json={"bar": "baz"},
        )


def test_error_message_stream(http_testserver, client):
    http_testserver.add_response("/api/400", {"foo": "bar"}, method="POST", status=400)
    request_kwargs = {
        "headers": {"Authorization": "Bearer foo", "X-Auth": "barbaz"},
        "params": {"param": 1},
        "json": {"bar": "baz"},
    }
    expected_error = rf"POST {http_testserver.url}/api/400\?param=1 - 400 BAD REQUEST"
    with pytest.raises(errors.ClientError, match=expected_error):
        with client.stream("POST", "/api/400", **request_kwargs):
            pass


@pytest.mark.anyio
async def test_async_error_message_stream(http_testserver, client):
    http_testserver.add_response("/api/400", {"foo": "bar"}, method="POST", status=400)
    request_kwargs = {
        "headers": {"Authorization": "Bearer foo", "X-Auth": "barbaz"},
        "params": {"param": 1},
        "json": {"bar": "baz"},
    }
    expected_error = rf"POST {http_testserver.url}/api/400\?param=1 - 400 BAD REQUEST"
    with pytest.raises(errors.ClientError, match=expected_error):
        async with client.astream("POST", "/api/400", **request_kwargs):
            pass


def test_error_message_truncate(http_testserver, client):
    http_testserver.add_response("/api/400", "a" * 2000, method="POST", status=400)
    expected_error = (
        rf"POST {http_testserver.url}/api/400\?param=1 - 400 BAD REQUEST"
        f"\nResponse: {'a' * 997}..."
    )
    with pytest.raises(errors.ClientError, match=expected_error):
        client.post(
            "/api/400",
            headers={"Authorization": "Bearer foo", "X-Auth": "barbaz"},
            params={"param": 1},
            json={"bar": "baz"},
        )


def test_error_message_truncate_binary(http_testserver, client):
    msg = ("a" * 2000).encode("utf-16")
    http_testserver.add_response("/api/400", msg, method="POST", status=400)
    expected_error = (
        rf"POST {http_testserver.url}/api/400?param=1 - 400 BAD REQUEST"
        f"\nResponse: {str(msg)[:97]}..."
    )
    with pytest.raises(errors.ClientError) as excinfo:
        client.post(
            "/api/400",
            headers={"Authorization": "Bearer foo", "X-Auth": "barbaz"},
            params={"param": 1},
            json={"bar": "baz"},
        )
    assert str(excinfo.value) == expected_error


def test_client_retry_error(client, core):
    client.retry_backoff_factor = 0
    client.retry_total = 3
    core.add_response("/api/bad-gateway-error", method="POST", status=502)
    expected_error = f"POST {core.url}/api/bad-gateway-error - 502 BAD GATEWAY"
    with pytest.raises(errors.ServerError, match=expected_error):
        client.post("/api/bad-gateway-error")
    assert len(core.request_log) == 4


@pytest.mark.anyio
async def test_async_client_retry_error(client, core):
    client.retry_backoff_factor = 0
    client.retry_total = 3
    core.add_response("/api/bad-gateway-error", method="POST", status=502)
    expected_error = f"POST {core.url}/api/bad-gateway-error - 502 BAD GATEWAY"
    with pytest.raises(errors.ServerError, match=expected_error):
        await client.apost("/api/bad-gateway-error")
    assert len(core.request_log) == 4


def test_client_raises_on_client_error(client, core):
    core.add_response("/api/client-error", "foo", method="POST", status=400)
    expected_error = f"POST {core.url}/api/client-error - 400 BAD REQUEST"
    with pytest.raises(errors.ClientError, match=expected_error):
        client.post("/api/client-error")


def test_client_raises_on_server_error(client, core):
    core.add_response("/api/server-error", "foo", method="POST", status=500)
    expected_error = f"POST {core.url}/api/server-error - 500 INTERNAL SERVER ERROR"
    with pytest.raises(errors.ServerError, match=expected_error):
        client.post("/api/server-error")


def test_client_raises_on_invalid_json_response(http_testserver, client):
    http_testserver.add_response("/api/invalid", "a" * 2000)
    expected = f"GET {http_testserver.url}/api/invalid"
    expected += f" - invalid JSON response: {'a' * 997}..."
    with pytest.raises(errors.InvalidJSONError, match=expected):
        client.get("/api/invalid")


def test_client_error_message_with_redirect(http_testserver, client):
    redirect = partial(flask.redirect, f"{http_testserver.url}/api/location")
    http_testserver.add_callback("/api/redirect", redirect)
    http_testserver.add_response("/api/location", status=400)
    expected_error = (
        f"GET {http_testserver.url}/api/redirect - 302 FOUND\n"
        f"GET {http_testserver.url}/api/location - 400 BAD REQUEST"
    )
    with pytest.raises(errors.ClientError, match=expected_error):
        client.get("/api/redirect")


def test_client_raises_on_name_resolution_error():
    client = FWClient(url="http://localhost.test", retry_total=0, defer_auth=True)
    expected_error = r"GET http://localhost.test/path - \[Errno -[23]\] .*"
    with pytest.raises(errors.ConnectError, match=expected_error):
        client.get("/path")


def test_client_raises_on_connection_abort():
    client = FWClient(url="http://localhost:9955", retry_total=0, defer_auth=True)
    expected_error = (
        r"GET http://localhost:9955/api/path - ("
        r"\[Errno 111\] Connection refused|"
        r"Remote end closed connection without response|"
        r"\[Errno 99\] Address not available)"
    )
    with pytest.raises(errors.ConnectError, match=expected_error):
        client.get("/api/path")


def test_client_raises_on_timeout(http_testserver):
    http_testserver.add_callback("/api/timeout", lambda: time.sleep(1))
    client = FWClient(
        url=http_testserver.url, timeout=0.01, retry_total=0, defer_auth=True
    )
    expected_error = f"GET {http_testserver.url}/api/timeout - timed out"
    with pytest.raises(errors.ReadTimeout, match=expected_error):
        client.get("/api/timeout")
